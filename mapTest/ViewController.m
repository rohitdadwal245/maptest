//
//  ViewController.m
//  mapTest
//
//  Created by clicklabs124 on 11/4/15.
//  Copyright (c) 2015 rohit. All rights reserved.
//

#import "ViewController.h"
@import GoogleMaps;
NSString *city;
#import <GoogleMaps/GoogleMaps.h>
@interface ViewController (){
   CLLocationManager *manager;
}

@property (weak, nonatomic) IBOutlet UIView *googlemapviewer;
@property (weak, nonatomic) IBOutlet UIButton *button1;

@end

@implementation ViewController
@synthesize googlemapviewer;


GMSMapView *mapView_;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
     [manager requestWhenInUseAuthorization];
    [manager startUpdatingLocation];
   

    
    
    
    
   
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateLocation %@",newLocation);
    position1=newLocation.coordinate;
    GMSMarker *marker=[GMSMarker markerWithPosition:position1];
    marker.map=googlemapviewer;
    
}
-(void)location
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:21.1700
                                                        longitude:72.8300];
    
    [geocoder reverseGeocodeLocation:newLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       
                       if (error) {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       
                       if (placemarks && placemarks.count > 0)
                       {
                           CLPlacemark *placemark = placemarks[0];
                           
                           NSDictionary *addressDictionary =
                           placemark.addressDictionary;
                           
                           NSLog(@"%@ ", addressDictionary);
                       }}];}

- (IBAction)GetDetail:(id)sender {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
